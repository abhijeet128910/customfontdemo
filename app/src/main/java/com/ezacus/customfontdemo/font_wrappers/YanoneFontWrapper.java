package com.ezacus.customfontdemo.font_wrappers;

import android.content.Context;
import android.graphics.Typeface;

/**
 * @author abhijeet.j
 */
public class YanoneFontWrapper {

    private static YanoneFontWrapper instance;
    private static Typeface typeface;

    public static YanoneFontWrapper getInstance(Context context) {
        synchronized (YanoneFontWrapper.class) {
            if (instance == null) {
                instance = new YanoneFontWrapper();
                typeface = Typeface.createFromAsset(context.getResources().getAssets(), "Yanone.ttf");
            }
            return instance;
        }
    }

    public Typeface getTypeFace() {
        return typeface;
    }
}