package com.ezacus.customfontdemo.font_wrappers;

import android.content.Context;
import android.graphics.Typeface;

/**
 * @author abhijeet.j
 */
public class YanoneBoldFontWrapper {

    private static YanoneBoldFontWrapper instance;
    private static Typeface typeface;

    public static YanoneBoldFontWrapper getInstance(Context context) {
        synchronized (YanoneBoldFontWrapper.class) {
            if (instance == null) {
                instance = new YanoneBoldFontWrapper();
                typeface = Typeface.createFromAsset(context.getResources().getAssets(), "Yanone_bold.ttf");
            }
            return instance;
        }
    }

    public Typeface getTypeFace() {
        return typeface;
    }
}