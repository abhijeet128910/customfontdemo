package com.ezacus.customfontdemo.font_wrappers;

import android.content.Context;
import android.graphics.Typeface;

/**
 * @author abhijeet.j
 */
public class CatLinzFontWrapper {

    private static CatLinzFontWrapper instance;
    private static Typeface typeface;

    public static CatLinzFontWrapper getInstance(Context context) {
        synchronized (CatLinzFontWrapper.class) {
            if (instance == null) {
                instance = new CatLinzFontWrapper();
                typeface = Typeface.createFromAsset(context.getResources().getAssets(), "CatLinz.ttf");
            }
            return instance;
        }
    }

    public Typeface getTypeFace() {
        return typeface;
    }
}