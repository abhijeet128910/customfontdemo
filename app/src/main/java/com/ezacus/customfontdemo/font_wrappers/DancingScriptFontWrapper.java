package com.ezacus.customfontdemo.font_wrappers;

import android.content.Context;
import android.graphics.Typeface;

/**
 * @author abhijeet.j
 */
public class DancingScriptFontWrapper {

    private static DancingScriptFontWrapper instance;
    private static Typeface typeface;

    public static DancingScriptFontWrapper getInstance(Context context) {
        synchronized (DancingScriptFontWrapper.class) {
            if (instance == null) {
                instance = new DancingScriptFontWrapper();
                typeface = Typeface.createFromAsset(context.getResources().getAssets(), "dancingscript_regular.otf");
            }
            return instance;
        }
    }

    public Typeface getTypeFace() {
        return typeface;
    }
}