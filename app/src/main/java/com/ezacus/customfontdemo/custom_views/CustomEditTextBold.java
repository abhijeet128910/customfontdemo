package com.ezacus.customfontdemo.custom_views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

import com.ezacus.customfontdemo.font_wrappers.MontserratFontWrapper;


/**
 * @author abhijeet.j
 */
public class CustomEditTextBold extends EditText {

    public CustomEditTextBold(Context context) {
        super(context);
        setTypeface(MontserratFontWrapper.getInstance(context).getTypeFace());
    }

    public CustomEditTextBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(MontserratFontWrapper.getInstance(context).getTypeFace());
    }

    public CustomEditTextBold(Context context, AttributeSet attrs,
                              int defStyle) {
        super(context, attrs, defStyle);
        setTypeface(MontserratFontWrapper.getInstance(context).getTypeFace());
    }

}